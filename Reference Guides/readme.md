# Reference Guides
Handy references to Python code.

# Content
- [Data Structures (Collections)](collections-rg.py)
- [Database (DDL/DML)](database-rg.sql)
- [Functions-1](functions1-rg.py)
- [Functions-2](functions2-rg.py)
- [Lambda Filter Map](lambda-filter-map-rg.py)
- [List Comprehension](list-comprehension-rg.py)
- [Truthy Falsey](truthy-falsey-rg.py)
